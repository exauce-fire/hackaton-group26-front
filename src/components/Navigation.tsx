import {useRouter} from "next/router";
import {usePathname} from "next/navigation";

const navigation = [
  {name: 'Dashboard', href: '/', current: true},
  {name: 'Courses', href: '/courses', current: false},
  {name: 'Chapters', href: '/chapters', current: false},
  {name: 'Sections', href: '/sections', current: false},
]

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

export default function Navigation() {
  const pathname = usePathname();
  return (
    <div className="ml-10 flex items-baseline space-x-4">
      {navigation.map((item) => (
        <a
          key={item.name}
          href={item.href}
          className={classNames(
            item.href === pathname
              ? 'bg-gray-900 text-white'
              : 'text-gray-300 hover:bg-gray-700 hover:text-white',
            'rounded-md px-3 py-2 text-sm font-medium'
          )}
          aria-current={item.current ? 'page' : undefined}
        >
          {item.name}
        </a>
      ))}
    </div>
  );
}
