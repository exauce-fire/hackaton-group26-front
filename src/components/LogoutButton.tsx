export default function LogoutButton() {
  function handleLogout() {
    console.log("Logout button clicked");
  }

  return (
    <button
      className="rounded-md px-3 py-2 text-sm font-medium bg-gray-800 text-white hover:bg-gray-900 focus:outline-none focus-visible:outline focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
      onClick={handleLogout}
    >
      Log out
    </button>
  );
}
