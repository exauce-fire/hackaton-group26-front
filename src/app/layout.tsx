import '../assets/globals.css'
import {Inter} from 'next/font/google'
import React from "react";

const inter = Inter({subsets: ['latin']})

export const metadata = {
  title: 'OCF',
  description: 'Open Source Course Builder',
}

export default function RootLayout({children}: {
  children: React.ReactNode
}) {
  return (
    <html className="h-full" lang="en">
      <body className={`h-full ${inter.className}`}>{children}</body>
    </html>
  )
}
