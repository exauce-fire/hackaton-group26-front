import {ArrowLeftIcon, PencilIcon} from "@heroicons/react/24/outline";
import Link from "next/link";
import { CloudArrowUpIcon, LockClosedIcon, ServerIcon, TrashIcon } from '@heroicons/react/20/solid'

const features = [
  {
    name: 'Push to deploy.',
    description:
      'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque, iste dolor cupiditate blanditiis ratione.',
    icon: CloudArrowUpIcon,
  },
  {
    name: 'SSL certificates.',
    description: 'Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo.',
    icon: LockClosedIcon,
  },
  {
    name: 'Database backups.',
    description: 'Ac tincidunt sapien vehicula erat auctor pellentesque rhoncus. Et magna sit morbi lobortis.',
    icon: ServerIcon,
  },
]

export default function SectionPage() {
  return (
    <>
      <header className="bg-white shadow">
        <div className="mx-auto max-w-7xl px-4 py-6 sm:px-6 lg:px-8 flex justify-between">
          <h1 className="text-3xl font-bold tracking-tight text-gray-900">Chapter 1</h1>
          <span className="hidden sm:block">
            <Link
              href={"/chapters/edit/1"}
              className="mr-6 inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
            >
            <PencilIcon className="-ml-0.5 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true"/>
            Edit
          </Link>
            <Link
              href={"/chapters"}
              className="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
            >
              <ArrowLeftIcon className="-ml-0.5 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true"/>
              Back
            </Link>
          </span>
        </div>
      </header>
      <main>
        <div className="flex justify-between mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
          <div className="lg:pr-8 lg:pt-4">
            <div className="lg:max-w-lg">
              <h2 className="text-base font-semibold leading-7 text-indigo-600">Deploy faster</h2>
              <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">A better workflow</p>
              <p className="mt-6 text-lg leading-8 text-gray-600">
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores impedit perferendis suscipit eaque,
                iste dolor cupiditate blanditiis ratione.
              </p>
              <dl className="mt-10 max-w-xl space-y-8 text-base leading-7 text-gray-600 lg:max-w-none">
                {features.map((feature) => (
                  <div key={feature.name} className="relative pl-9">
                    <dt className="inline font-semibold text-gray-900">
                      <feature.icon className="absolute left-1 top-1 h-5 w-5 text-indigo-600" aria-hidden="true" />
                      {feature.name}
                    </dt>{' '}
                    <dd className="inline">{feature.description}</dd>
                  </div>
                ))}
              </dl>
            </div>
          </div>
          <div className="mt-8 font-bold">
            <fieldset>
              <legend className="text-sm font-semibold leading-6 text-gray-900">Sections</legend>
              <h2 className="text-base font-semibold leading-7 text-gray-900 mt-6">Section 1</h2>
              <p className="mt-1 text-sm leading-6 text-gray-600">
                This is section 1 title.
              </p>
              <h2 className="text-base font-semibold leading-7 text-gray-900 mt-4">Section 2</h2>
              <p className="mt-1 text-sm leading-6 text-gray-600">
                This is section 2 title.
              </p>
              <h2 className="text-base font-semibold leading-7 text-gray-900 mt-4">Section 3</h2>
              <p className="mt-1 text-sm leading-6 text-gray-600">
                This is section 3 title.
              </p>
            </fieldset>
          </div>
        </div>
      </main>
    </>
  );
}
