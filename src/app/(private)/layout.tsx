import React from "react";
import TopBar from "@/components/TopBar";

export default function RootLayout({children}: {
  children: React.ReactNode
}) {
  return (
    <div className="min-h-full">
      <TopBar />
      {children}
    </div>
  )
}
