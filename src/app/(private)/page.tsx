import Link from "next/link";

const courses = [
  {
    name: 'Course 1',
    email: 'Title 1',
    role: 'Chapter 1, Chapter 2',
    imageUrl:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
    lastSeenDateTime: '2023-01-23T13:23Z',
  },
  {
    name: 'Course 2',
    email: 'Title 2',
    role: 'Chapter 1',
    imageUrl:
      'https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
    lastSeenDateTime: '2023-01-23T13:23Z',
  },
  {
    name: 'Course 3',
    email: 'Title 3',
    role: 'Chapter 1',
    imageUrl:
      'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
  },
]

const chapters = [
  {
    name: 'Chapter 1',
    email: 'Title 1',
    role: 'Course 1, Course 2',
    imageUrl:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
    lastSeenDateTime: '2023-01-23T13:23Z',
  },
  {
    name: 'Chapter 2',
    email: 'Title 2',
    role: 'Course 1',
    imageUrl:
      'https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
    lastSeenDateTime: '2023-01-23T13:23Z',
  },
  {
    name: 'Chapter 3',
    email: 'Title 3',
    role: 'Course 1',
    imageUrl:
      'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
  },
]

const sections = [
  {
    name: 'Section 1',
    email: 'Title 1',
    role: 'Chapter 1',
    imageUrl:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
    lastSeenDateTime: '2023-01-23T13:23Z',
  },
  {
    name: 'Section 2',
    email: 'Title 2',
    role: 'Chapter 1',
    imageUrl:
      'https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
    lastSeenDateTime: '2023-01-23T13:23Z',
  },
  {
    name: 'Section 3',
    email: 'Title 3',
    role: 'Chapter 1',
    imageUrl:
      'https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
    lastSeen: '3h ago',
  },
]

export default function HomePage() {
  return (
    <>
      <header className="bg-white shadow">
        <div className="mx-auto max-w-7xl px-4 py-6 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-bold tracking-tight text-gray-900">Dashboard</h1>
        </div>
      </header>
      <main>
        <div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
          <h1 className="text-xl font-bold tracking-tight text-gray-900">Recent courses</h1>
          <ul role="list" className="divide-y divide-gray-100">
          {courses.map((course) => (
            <li key={course.email}>
              <Link className="flex justify-between gap-x-6 py-5" href={"/courses/1"}>
                <div className="flex gap-x-4">
                  <div className="min-w-0 flex-auto">
                    <p className="text-sm font-semibold leading-6 text-gray-900">{course.name}</p>
                    <p className="mt-1 truncate text-xs leading-5 text-gray-500">{course.email}</p>
                  </div>
                </div>
                <div className="hidden sm:flex sm:flex-col sm:items-end">
                  <p className="text-sm leading-6 text-gray-900">{course.role}</p>
                  {course.lastSeen ? (
                    <p className="mt-1 text-xs leading-5 text-gray-500">
                      Last modified <time dateTime={course.lastSeenDateTime}>{course.lastSeen}</time>
                    </p>
                  ) : (
                    <div className="mt-1 flex items-center gap-x-1.5">
                      <div className="flex-none rounded-full bg-emerald-500/20 p-1">
                        <div className="h-1.5 w-1.5 rounded-full bg-emerald-500" />
                      </div>
                      <p className="text-xs leading-5 text-gray-500">Online</p>
                    </div>
                  )}
                </div>
              </Link>
            </li>
          ))}
          </ul>
          <h1 className="text-xl font-bold tracking-tight text-gray-900 mt-6">Recent chapters</h1>
          <ul role="list" className="divide-y divide-gray-100">
            {chapters.map((chapter) => (
              <li key={chapter.email}>
                <Link className="flex justify-between gap-x-6 py-5" href={"/chapters/1"}>
                  <div className="flex gap-x-4">
                    <div className="min-w-0 flex-auto">
                      <p className="text-sm font-semibold leading-6 text-gray-900">{chapter.name}</p>
                      <p className="mt-1 truncate text-xs leading-5 text-gray-500">{chapter.email}</p>
                    </div>
                  </div>
                  <div className="hidden sm:flex sm:flex-col sm:items-end">
                    <p className="text-sm leading-6 text-gray-900">{chapter.role}</p>
                    {chapter.lastSeen ? (
                      <p className="mt-1 text-xs leading-5 text-gray-500">
                        Last modified <time dateTime={chapter.lastSeenDateTime}>{chapter.lastSeen}</time>
                      </p>
                    ) : (
                      <div className="mt-1 flex items-center gap-x-1.5">
                        <div className="flex-none rounded-full bg-emerald-500/20 p-1">
                          <div className="h-1.5 w-1.5 rounded-full bg-emerald-500" />
                        </div>
                        <p className="text-xs leading-5 text-gray-500">Online</p>
                      </div>
                    )}
                  </div>
                </Link>
              </li>
            ))}
          </ul>
          <h1 className="text-xl font-bold tracking-tight text-gray-900 mt-6">Recent sections</h1>
            <ul role="list" className="divide-y divide-gray-100">
              {sections.map((section) => (
                <li key={section.email}>
                  <Link className="flex justify-between gap-x-6 py-5" href={"/sections/1"}>
                    <div className="flex gap-x-4">
                      <div className="min-w-0 flex-auto">
                        <p className="text-sm font-semibold leading-6 text-gray-900">{section.name}</p>
                        <p className="mt-1 truncate text-xs leading-5 text-gray-500">{section.email}</p>
                      </div>
                    </div>
                    <div className="hidden sm:flex sm:flex-col sm:items-end">
                      <p className="text-sm leading-6 text-gray-900">{section.role}</p>
                      {section.lastSeen ? (
                        <p className="mt-1 text-xs leading-5 text-gray-500">
                          Last modified <time dateTime={section.lastSeenDateTime}>{section.lastSeen}</time>
                        </p>
                      ) : (
                        <div className="mt-1 flex items-center gap-x-1.5">
                          <div className="flex-none rounded-full bg-emerald-500/20 p-1">
                            <div className="h-1.5 w-1.5 rounded-full bg-emerald-500" />
                          </div>
                          <p className="text-xs leading-5 text-gray-500">Online</p>
                        </div>
                      )}
                    </div>
                  </Link>
                </li>
              ))}
            </ul>
        </div>
      </main>
    </>
  );
}
